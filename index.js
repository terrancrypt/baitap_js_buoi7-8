document.querySelector("#btnThemSo").onclick = taoMang;

// Tạo mảng
var arr = [];
function taoMang() {
  const nhapSo = Number(document.getElementById("iSo").value);
  document.getElementById("iSo").value = "";
  arr.push(nhapSo);
  document.querySelector("#ketQua1").innerHTML = `${arr}`;
  console.log(arr);
}

document.querySelector("#btnTinhTong").onclick = tongSoDuong;
// Tổng số dương
function tongSoDuong() {
  var count = 0;
  for (i = 0; i < arr.length; i++) {
    const num = arr[i];
    if (num > 0) {
      count += num;
    }
  }
  document.getElementById("ketQua2").innerHTML = count;
}

document.querySelector("#btnDemSo").onclick = demSo;
// Đếm số dương
function demSo() {
  var soDem = 0;
  for (var i = 0; i < arr.length; i++) {
    const num = arr[i];
    if (num >= 0) {
      soDem++;
    }
  }
  document.getElementById("ketQua3").innerHTML = soDem;
}

document.querySelector("#btnTimSoNhoNhat").onclick = timSoNhoNhat;
// Tìm số nhỏ nhất trong mảng
function timSoNhoNhat() {
  // Giả định vị trí số nhỏ nhất là số đầu tiên của mảng
  var min = arr[0];
  // Tạo vòng lặp so sánh từng số trong mảng với giá trị đầu tiên để tìm ra giá trị nhỏ nhất
  for (var i = 1; i < arr.length; i++) {
    if (min > arr[i]) {
      // thay đổi giá trị nhỏ nhất nếu tìm ra số nhỏ hơn
      min = arr[i];
    }
  }

  document.getElementById("ketQua4").innerHTML = min;
}

// Tìm số dương nhỏ nhất
document.querySelector("#btnTimSoDuongNhoNhat").onclick = timSoDuongNhoNhat;
function timSoDuongNhoNhat() {
  // Tạo một mảng mới dùng để chứa các số dương
  var arrDuong = [];
  // Dùng vòng lặp để lấy các số dương từ mảng cũ và lưu vào trong mảng mới
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      arrDuong.push(arr[i]);
    }
  }
  // Kiểm tra mảng mới có phần tử hay không bằng cách kiểm tra độ dài phần tử của mảng
  if (arrDuong.length > 0) {
    // Giả định vị trí số nhỏ nhất là só đầu tiên của mảng dương mới
    var min = arrDuong[0];
    for (var i = 0; i < arrDuong.length; i++) {
      if (min > arrDuong[i]) {
        min = arrDuong[i];
      }
    }
  } else {
    min = "Mảng không có số dương";
  }

  document.getElementById("ketQua5").innerHTML = min;
}

// Tìm số chẵn cuối cùng
document.querySelector("#btnTimSoChan").onclick = timSoChanCuoi;
function timSoChanCuoi() {
  // Tạo một biến chứa số chẵn cuối cùng
  var soChan = 0;
  // Dùng vòng lặp để tìm ra các số chẵn trong mảng
  for (i = 0; i < arr.length; i++) {
    if (arr[i] % 2 === 0) {
      soChan = arr[i];
    }
  }
  document.getElementById("ketQua6").innerHTML = "Số chẵn cuối cùng: " + soChan;
}

// Đổi chỗ
document.querySelector("#btnDoiCho").onclick = doiCho;
function doiCho() {
  // lấy input từ giao diện
  var viTri1 = document.getElementById("soViTri1").value * 1;
  var viTri2 = document.getElementById("soViTri2").value * 1;

  // tạo một biến tạm, gán trái trị của phần tử ở vị trí 1 vào biến tạm
  var indexChange = arr[viTri1];

  // Gán giá trị của phần tử ở vị trí 2 vào vị trí 1
  arr[viTri1] = arr[viTri2];

  // Gán giá trị của biến tạm vào vị trí 2
  arr[viTri2] = indexChange;

  document.getElementById("ketQua7").innerHTML =
    "Mảng sau khi đổi: " + `${arr}`;
}

// Sắp xếp tăng dần
document.querySelector("#btnSapXepTangDan").onclick = sapXepTangDan;
function sapXepTangDan() {
  // Sử dụng hàm compareNumbers để sắp xếp lại mảng theo thứ tự từ bé đến lớn
  function compareNumbers(a, b) {
    return a - b;
  }
  ketQua = arr.sort(compareNumbers);
  document.getElementById("ketQua8").innerHTML =
    "Mảng sau khi sắp xếp: " + `${arr}`;
}

// Tìm số nguyên tố đầu tiên trong mảng
document.querySelector("#btnTimSoNguyenTo").onclick = timSoNguyenTo;
function timSoNguyenTo() {
  // Tạo một vòng lặp để check số nguyên tố từng giá trị trong mảng
  for (i = 0; i < arr.length; i++) {
    // Tạo biến checkSNT, mặc định true
    checkSNT = true;
    for (index = 2; index <= Math.sqrt(arr[i]); index++) {
      // Nếu giá trị trong mảng đang kiểm tra chia hết số nào khác ngoài nó thì trả về giá trị false
      if (arr[i] % index === 0) {
        checkSNT = false;
      }
    }
    // Nếu check giá trị trả về là true và khác 1 thì kết quả là số nguyên tố đầu tiên và break ra khỏi vòng lặp
    if (checkSNT === true && arr[i] !== 1) {
      ketQua = arr[i];
      break;
    } else {
      ketQua = -1;
    }
  }
  document.getElementById("ketQua9").innerHTML = ketQua;
}

// Đếm số nguyên
document.querySelector("#btnDemSoNguyen").onclick = demSoNguyen;

// Tạo thêm 1 mảng chứa số thực
var arrSoThuc = [];
document.querySelector("#btnThemSoThuc").onclick = function () {
  const soThuc = Number(document.getElementById("arraySoThuc").value);
  document.getElementById("arraySoThuc").value = "";
  arrSoThuc.push(soThuc);
  document.getElementById("arraySo").innerHTML = `${arrSoThuc}`;
};
function demSoNguyen() {
  // Tạo một mảng chứa kết quả
  arrKetQua = [];
  // Dùng vòng lặp for kiểm tra mảng arrSoThuc, nếu có số nào là số nguyên thì thêm vào mảng arrKetQua
  for (i = 0; i < arrSoThuc.length; i++) {
    if (Number.isInteger(arrSoThuc[i]) == true) {
      arrKetQua.push(arrSoThuc[i]);
    }
  }
  //In ra màn hình kết quả là chiều dài của mảng arrKetQua
  document.getElementById("ketQua10").innerHTML =
    "Số nguyên: " + arrKetQua.length;
}

// So sánh số lượng số dương và âm
document.querySelector("#btnSoSanhSo").onclick = soSanhSo;

function soSanhSo() {
  // Tao 2 mảng phụ chứa số âm và số dương
  arrSoDuong = [];
  arrSoAm = [];
  // Dùng hàm for check mảng chính arr và push giá trị vào 2 mảng phụ
  for (i = 0; i < arr.length; i++) {
    // Nếu giá trị trong mảng lớn hơn 0 thì push giá trị vào arrSoDuong, còn không thì ngược lại
    if (arr[i] >= 0) {
      arrSoDuong.push(arr[i]);
    } else {
      arrSoAm.push(arr[i]);
    }
  }
  // Dùng lệnh if esle so sánh chiều dài của 2 mảng phụ
  if (arrSoDuong.length > arrSoAm.length) {
    ketQua = "Số dương > Số Âm";
  } else if (arrSoDuong.length == arrSoAm.length) {
    ketQua = "Số dương = Số Âm";
  } else {
    ketQua = "Số dương < Số Âm";
  }

  document.getElementById("ketQua11").innerHTML = ketQua;
}
